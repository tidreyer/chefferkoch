function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function chefferkoch() {
    console.log("chefferkoch start");

    var main = document.body.getElementsByTagName("MAIN")[0];
    var articles = main.getElementsByTagName("ARTICLE");
    // removing nutrition values
    for (var i = articles.length-1; i >= 0; i--) {
        var elem = articles[i];
        if (elem.classList.contains("recipe-nutrition")) {
            elem.remove();
        }
    }
    var header = articles[0];
    var ingred = articles[1];
    var steps = articles[2];
    var comments = articles[3];

    console.log("chefferkoch removing adds and clutter");

    var bodyChildren = document.body.children;
    for (var i = bodyChildren.length-1; i >= 0; i--) {
        var child = bodyChildren[i];
        if (child.tagName != "MAIN") {
            child.remove();
        }
    }

    var mainChildren = main.children;
    for (var i = mainChildren.length-1; i >= 0; i--) {
        var child = mainChildren[i];
        if (child.tagName != "ARTICLE") {
            child.remove();
        }
    }

    console.log("chefferkoch restructure page");

    main.style.padding = "0";
    main.style.margin = "0";
    main.style.width = "100%";
    main.style.maxWidth = "100%";
    main.style.display = "grid";
    main.style.gridTemplateColumns = "auto auto";
    main.style.gridColumnGap = "10px";
    for (var i=0; i < articles.length; i++) {
        var art = articles[i];
        art.style.position = "relative";
        art.style.padding = 0;
        art.style.margin = 0;
        art.style.width = "100%";
        art.style.maxWidth = "100%";
    }
    header.style.gridColumn = "1 / span 2";
    ingred.style.gridColumn = "1 / span 1";
    steps.style.gridColumn = "2 / span 1";
    comments.style.gridColumn = "1 / span 2";

    var image = header.getElementsByClassName("recipe-image")[0];
    image.style.maxWidth = "800px";

    console.log("chefferkoch end");
}

async function main() {
    for (let i = 0; i<10; i++) {
        await sleep(1000);
        chefferkoch();
    }
}

main();
