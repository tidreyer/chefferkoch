# ChefferKoch Firefox Extension

This extension modifies the display of recepies on chefkoch.de to have a
cleaner view, with the ingredient list to the left of the preparation
description.

![Screenshots demonstrating the effect of the add-on](img/demo-image_arrows.png)
